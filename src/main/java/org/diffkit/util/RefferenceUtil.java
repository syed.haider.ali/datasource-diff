package org.diffkit.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public class RefferenceUtil<T> {
     T bind(Map refMap, T data) {
        refMap.forEach(
                (key,value)->{
                 if(value instanceof String){
                     Object bean= DKSpringUtil.getBean((String) value);
                     if(bean !=null){
                         try {
                             callSetter(data,bean,(String) key);
                         } catch (NoSuchMethodException |InvocationTargetException |IllegalAccessException e) {
                             throw new IllegalArgumentException("Reference not found");
                         }
                     }else {
                         throw new IllegalArgumentException("Reference bean not found");
                     }
                 }else
                 {
                     throw new IllegalArgumentException("Deep hierarchy not supported yet");
                 }
                }
        );
        return data;
    }

    private void callSetter(T data,Object bean,String field) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
         String setterName= "set" +  (field.length()> 1? field.substring(0,0).toUpperCase()+field.substring(1,field.length()-1):field.substring(0,0).toUpperCase());
         Method setter=data.getClass().getDeclaredMethod(setterName,bean.getClass());
         setter.invoke(data,bean);
    }
}
