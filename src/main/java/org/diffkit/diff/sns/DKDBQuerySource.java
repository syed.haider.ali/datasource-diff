package org.diffkit.diff.sns;

import org.apache.commons.lang.NotImplementedException;
import org.diffkit.common.DKValidate;
import org.diffkit.common.annot.NotThreadSafe;
import org.diffkit.db.*;
import org.diffkit.diff.engine.DKContext;
import org.diffkit.diff.engine.DKSource;
import org.diffkit.diff.engine.DKTableModel;
import org.diffkit.util.DKSqlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.sql.*;

@NotThreadSafe
public class DKDBQuerySource implements DKSource {

    private static final int DEFAULT_FETCH_SIZE = 1000;
    private final DKDatabase _database;
    private final String tableName_;
    private final String _query;
    private String[] _readColumnNames;
    private DKSqlUtil.ReadType[] _readTypes;
    private final DKTableModel _model;
    private final DKDBTable _table;
    private final String[] _keyColumnNames;
    private transient Connection _connection;
    private transient ResultSet _resultSet;
    private transient long _lastIndex;
    private transient boolean _isOpen;
    private transient boolean _rsIsConsumed;

    private final Logger _log = LoggerFactory.getLogger(this.getClass());
    private final boolean _isDebug = _log.isDebugEnabled();

    public DKDBQuerySource(String query_, DKDatabase database_,
                      DKTableModel model_, String[] keyColumnNames_, int[] readColumnIdxs_)
            throws SQLException {
        _log.debug("whereClause_->{}", query_);
        _log.debug("database_->{}", database_);
        _log.debug("model_->{}", model_);
        _log.debug("keyColumnNames_->{}", keyColumnNames_);
        _log.debug("readColumnIdxs_->{}", readColumnIdxs_);

        if (readColumnIdxs_ != null)
            throw new NotImplementedException("readColumnIdxs_ not yet implemented");
        if ((model_ != null) && (keyColumnNames_ != null))
            throw new RuntimeException(String.format("does not allow both %s and %s params",
                    "model_", "keyColumnNames_"));
        this._query = query_;
        _keyColumnNames = keyColumnNames_;
        _database = database_;
        DKValidate.notNull(_database);
        tableName_="DEFAULT";
        _table = this.getDefaultTable();
        _log.debug("table->{}", _table);
        if (_table == null)
            throw new RuntimeException(String.format("couldn't find table named->%s",
                    tableName_));
        _model = getModel();
        _log.debug("_model->{}", _model);
        DKValidate.notNull(_model);
    }

    public String getQuery() {
        return _query;
    }

    @Override
    public DKTableModel getModel() {
        if (_model != null)
            return _model;
        try {
            return DKTableModelUtil.createDefaultTableModel(_database.getFlavor(), _table,
                    _keyColumnNames);
        }
        catch (Exception e_) {
            throw new RuntimeException(e_);
        }
    }

    @Override
    public URI getURI() throws IOException {
        return null;
    }

    @Override
    public Object[] getNextRow() throws IOException {
        try {
            this.ensureOpen();
            if (_rsIsConsumed)
                return null;
            if (!_resultSet.next()) {
                _rsIsConsumed = true;
                return null;
            }
            _lastIndex++;
            return DKSqlUtil.readRow(_resultSet, _readColumnNames, _readTypes);
        }
        catch (Exception e_) {
            throw new RuntimeException(e_);
        }
    }

    @Override
    public long getLastIndex() {
        return _lastIndex;
    }

    @Override
    public void open(DKContext context_) throws IOException {
        this.ensureNotOpen();
        try {
            _readColumnNames = _model.getColumnNames();
            _readTypes = _table.getReadTypes(_readColumnNames, _database);
            _connection = _database.getConnection();
            if (_database.getFlavor() != DKDBFlavor.DB2)
                _connection.setAutoCommit(false);
            _resultSet = this.createResultSet();
            if (_isDebug)
                _log.debug("_resultSet->{}", _resultSet);
            _lastIndex = -1;
            _rsIsConsumed = false;
            _isOpen = true;
        }
        catch (Exception e_) {
            _log.error(null, e_);
            _connection = null;
            _resultSet = null;
            _isOpen = false;
            throw new RuntimeException(e_);
        }
    }

    @Override
    public void close(DKContext context_) throws IOException {
        this.ensureOpen();
        DKSqlUtil.close(_resultSet);
        DKSqlUtil.close(_connection);
        _resultSet = null;
        _connection = null;
        _isOpen = false;
        _rsIsConsumed = true;
    }

    @Override
    public Kind getKind() {
        return Kind.DB;
    }

    private ResultSet createResultSet() throws SQLException {
        return DKSqlUtil.executeQuery(this.getQuery(), _connection,
                DEFAULT_FETCH_SIZE);
    }

    public  DKDBTable getDefaultTable() throws SQLException {
        Statement statement=_database.getConnection().createStatement();
        statement.setMaxRows(1);
        ResultSet rs=statement.executeQuery(_query);
        ResultSetMetaData rsMetaData = rs.getMetaData();
        int numberOfColumns = rsMetaData.getColumnCount();
        DKDBColumn[] columns = new DKDBColumn[rsMetaData.getColumnCount()];
        for(int i=1;i<=rsMetaData.getColumnCount();i++){
            columns[i-1] = new DKDBColumn(rsMetaData.getColumnName(i), i,
                    rsMetaData.getColumnTypeName(i), rsMetaData.getColumnDisplaySize(i), rsMetaData.isNullable(i)==1);
        }
        DKDBPrimaryKey primaryKey =new DKDBPrimaryKey("defaultKey", _keyColumnNames);
        return new DKDBTable(null, null, tableName_, columns, primaryKey);
    }
    private void ensureOpen() {
        if (!_isOpen)
            throw new RuntimeException("not open!");
    }

    private void ensureNotOpen() {
        if (_isOpen)
            throw new RuntimeException("already open!");
    }
}
