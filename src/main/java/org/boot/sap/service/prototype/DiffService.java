package org.boot.sap.service.prototype;

import org.assertj.core.util.Arrays;
import org.diffkit.diff.conf.DKApplication;
import org.diffkit.diff.conf.DKPlan;
import org.diffkit.diff.engine.*;
import org.diffkit.util.DKMapUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/*
*Keeping scope prototype to create new bean ech time
*/
@Service
@Scope("prototype")
public class DiffService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiffService.class);
    public void diff(){
        DKApplication.main(Arrays.array("-planfiles","D:\\sharedData\\projects\\gitRepo\\DiffKit\\src\\test\\resources\\org\\diffkit\\diff\\testcase\\test9.plan.xml"));
    }

    public void diff(DKPlan plan) throws Exception{
        LOGGER.info("plan->{}", plan);
        DKSource lhsSource = plan.getLhsSource();
        DKSource rhsSource = plan.getRhsSource();
        DKSink sink = plan.getSink();
        DKTableComparison tableComparison = plan.getTableComparison();
        LOGGER.info("lhsSource->{}", lhsSource);
        LOGGER.info("rhsSource->{}", rhsSource);
        LOGGER.info("sink->{}", sink);
        LOGGER.info("tableComparison->{}", tableComparison);
        Map<DKContext.UserKey, Object> userDictionary = new HashMap<DKContext.UserKey, Object>();
        //userDictionary.put(DKContext.UserKey.PLAN_FILES, planFilesString_);
        DKContext diffContext = doDiff(lhsSource, rhsSource, sink, tableComparison,
                userDictionary);
        LOGGER.info(sink.generateSummary(diffContext));
    }

    private static DKContext doDiff(DKSource lhsSource_, DKSource rhsSource_,
                                    DKSink sink_, DKTableComparison tableComparison_,
                                    Map<DKContext.UserKey, Object> userDictionary_) throws Exception {
        DKDiffEngine engine = new DKDiffEngine();
        userDictionary_ = DKMapUtil.combine(userDictionary_,
                tableComparison_.getUserDictionary());
        LOGGER.info("engine->{}", engine);
        return engine.diff(lhsSource_, rhsSource_, sink_, tableComparison_, userDictionary_);
    }
}
