package org.boot.sap.dto;

import org.diffkit.util.RefferenceUtil;

import java.util.Map;

//Make it immuateble
public class RefDTO<T> {
    private T data;
    private Map refMap;

    public Map getRefMap() {
        return refMap;
    }

    public void setRefMap(Map refMap) {
        this.refMap = refMap;
    }

    public T getData() {
        return bind(refMap,data);
    }

    public void setT(T t) {
        this.data = t;
    }

    private T bind(Map refMap,T data){
        return data;
    }
}
