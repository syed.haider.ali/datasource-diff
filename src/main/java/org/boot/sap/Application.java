package org.boot.sap;

import org.boot.sap.entity.Person;
import org.boot.sap.entity.PersonRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }

    @Bean
    InitializingBean savePersons(final PersonRepository personRepository){
        return () ->{
            Person person1=new Person("Haider","Ali");
            Person person2=new Person("Aqib","Syed");
            personRepository.save(person1);
            personRepository.save(person2);
        };
    }
}
