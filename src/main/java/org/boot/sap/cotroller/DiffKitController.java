package org.boot.sap.cotroller;


import org.boot.sap.dto.MagicPlanDTO;
import org.boot.sap.service.prototype.DiffService;
import org.diffkit.diff.conf.DKMagicPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 *Keeping scope request to create new bean ech time
 */
@RestController
@Scope("request")
@RequestMapping("/diff")
public class DiffKitController {

    @Autowired
    DiffService diffService;

    @GetMapping("/do")
    public Map<String,String> doMethod(){
        HashMap result= new HashMap();
        try{
            diffService.diff();
            result.put("status","success");
        }catch (Exception e){
            result.put("status","failed");
            result.put("message",e.getMessage());
            result.put("exception",e.getClass().getSimpleName());
            e.printStackTrace();
        }
        return result;
    }

    @PostMapping("/do")
    public Map<String,Object> doMethod(@RequestBody MagicPlanDTO planDTO){
        HashMap result= new HashMap();
        try{
            Long time=System.currentTimeMillis();
            diffService.diff(planDTO.getData());
            result.put("status","success");
            result.put("time",System.currentTimeMillis()-time);
            System.out.println("Time :: "+ (System.currentTimeMillis()-time));
        }catch (Exception e){
            result.put("status","failed");
            result.put("message",e.getMessage());
            result.put("exception",e.getClass().getSimpleName());
            e.printStackTrace();
        }
        return result;
    }
}
